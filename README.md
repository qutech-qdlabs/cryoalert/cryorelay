# CryoRelay

The CryoRelay is part of the CryoAlert platform. It is a relatively simple application that collectors and forwards alert-collections to the various fridges connected to the relay on which CryoRelay is installed, when such an alert-collection is updated by a user within CryoAdmin. See the [documentation](./docs/index.md) for a detailed description.
