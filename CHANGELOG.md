# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.3] - 2024-06-27

### Added

### Fixed

### Changed

- Changed default setting for `cryo_admin_url` when initializing

### Removed

## [1.0.2] - 2024-06-13

### Fixed

- Fixed command in run script

## [1.0.1] - 2024-06-13

### Fixed

- Added missing run script

## [1.0.0] - 2024-06-12

### Added

- Initial release

### Fixed

### Changed

### Removed
