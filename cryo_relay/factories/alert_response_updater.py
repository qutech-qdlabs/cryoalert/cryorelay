import logging
import multiprocessing as mp

from cryo_relay.processes.alert_response_updater import AlertResponseUpdaterProcess
from cryo_relay.utils.config import AlertUpdaterConfig
from cryo_relay.utils.factory_management import AbstractFactory

LOGGER = logging.getLogger(__name__)


class AlertResponseUpdaterFactory(AbstractFactory):
    def __init__(self, log_queue: mp.Queue, *args, **kwargs) -> None:
        self.log_queue = log_queue
        self.args = args
        self.kwargs = kwargs

    def create(self, idx: int, config: AlertUpdaterConfig) -> AlertResponseUpdaterProcess:
        """
        Factory method used to create a single AlertResponseUpdaterProcess.
        When needing multiple, it should be arranged by the Manager.

        Args:
            idx (int): Index of the AlertUpdaterProcess
            config (AlertUpdaterConfig): Configuration for the AlertUpdaterProcess

        Returns:
            AlertResponseUpdaterProcess: The created Process
        """
        return AlertResponseUpdaterProcess(
            idx=idx,
            config=config,
            log_queue=self.log_queue,
            *self.args,
            **self.kwargs,
        )
