import logging
import multiprocessing as mp

from cryo_relay.processes.alert_updater import AlertUpdaterProcess
from cryo_relay.utils.config import AlertUpdaterConfig
from cryo_relay.utils.factory_management import AbstractFactory

LOGGER = logging.getLogger(__name__)


class AlertUpdaterFactory(AbstractFactory):
    def __init__(self, log_queue: mp.Queue, *args, **kwargs) -> None:
        self.log_queue = log_queue
        self.args = args
        self.kwargs = kwargs

    def create(self, idx: int, config: AlertUpdaterConfig) -> AlertUpdaterProcess:
        """
        Factory method used to create a single AlertUpdaterProcess.
        When needing multiple, it should be arranged by the Manager.

        Args:
            idx (int): Index of the AlertUpdaterProcess
            config (AlertUpdaterConfig): Configuration for the AlertUpdaterProcess

        Returns:
            AlertUpdaterProcess: The created Process
        """
        return AlertUpdaterProcess(
            idx=idx,
            config=config,
            log_queue=self.log_queue,
            *self.args,
            **self.kwargs,
        )
