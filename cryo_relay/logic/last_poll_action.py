import logging
from datetime import datetime

from sqlalchemy.orm import Session

from cryo_relay.models import LastPollAction

LOGGER = logging.getLogger(__name__)


def get_latest_poll_action(session: Session) -> LastPollAction | None:
    return session.query(LastPollAction).first()


def update_latest_poll_action(session: Session, new_poll_time: datetime) -> None:
    latest_poll_action = get_latest_poll_action(session)
    if latest_poll_action:
        latest_poll_action.timestamp = new_poll_time
    else:
        new_latest_poll_action = LastPollAction(timestamp=new_poll_time)
        session.add(new_latest_poll_action)

    session.commit()
