import logging
from datetime import UTC, datetime

from sqlalchemy.orm import Session

from cryo_relay.models import UpdateResponseQueue
from cryo_relay.utils.types import AlertUpdateResponse

LOGGER = logging.getLogger(__name__)


def get_latest_update_response(session: Session, limit: int = 1, lock: bool = False) -> list[UpdateResponseQueue]:
    """
    Fetch a specified number of unlocked response_updates

    Args:
        session (Session): active database session
        limit (int, optional): Number of response_updates to fetch. Defaults to 1.
        lock (bool, optional): Flag to set retrieved response_updates to be locked. Defaults to False.

    Returns:
        list[UpdateResponseQueue]: List of fetched UpdateResponses
    """
    response_updates = (
        session.query(UpdateResponseQueue)
        .filter(UpdateResponseQueue.locked == False)  # pylint: disable=singleton-comparison
        .order_by(UpdateResponseQueue.timestamp)
        .limit(limit)
        .all()
    )
    if lock:
        for response_update in response_updates:
            response_update.locked = True
            response_update.locked_timestamp = datetime.now(UTC)
        session.commit()

    return response_updates


def add_latest_update_response(session: Session, data: AlertUpdateResponse, update_uuid: str) -> None:
    new_update_response = UpdateResponseQueue(
        timestamp=datetime.now(UTC),
        uuid=update_uuid,
        success=data["ok"],
        error=data.get("error_message", ""),
    )
    session.add(new_update_response)
    session.commit()


def delete_response_updates(session: Session, update_responses: list[UpdateResponseQueue]) -> None:
    for update_response in update_responses:
        session.delete(update_response)
    session.commit()


def unlock_response_updates(session: Session, update_responses: list[UpdateResponseQueue]) -> None:
    for update_response in update_responses:
        update_response.locked = False
        update_response.locked_timestamp = None
    session.commit()
