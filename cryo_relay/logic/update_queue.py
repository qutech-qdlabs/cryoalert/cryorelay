import logging
from datetime import UTC, datetime, timedelta

from sqlalchemy.orm import Session

from cryo_relay.models import UpdateQueue
from cryo_relay.utils.types import AlertDefinitionUpdate

LOGGER = logging.getLogger(__name__)


def get_latest_updates(session: Session, limit: int = 1, lock: bool = False) -> list[UpdateQueue]:
    """
    Fetch a specified number of unlocked updates

    Args:
        session (Session): active database session
        limit (int, optional): Number of updates to fetch. Defaults to 1.
        lock (bool, optional): Flag to set retrieved updates to be locked. Defaults to False.

    Returns:
        list[UpdateQueue]: List of fetched Updates
    """
    updates = (
        session.query(UpdateQueue)
        .filter(UpdateQueue.locked == False)  # pylint: disable=singleton-comparison
        .order_by(UpdateQueue.timestamp)
        .limit(limit)
        .all()
    )
    if lock:
        for update in updates:
            update.locked = True
            update.locked_timestamp = datetime.now(UTC)
        session.commit()

    return updates


def add_latest_update(session: Session, data: AlertDefinitionUpdate) -> None:
    # First, check if the uuid already exists. if so, overwrite the alert definition data
    prev_update = session.query(UpdateQueue).filter(UpdateQueue.uuid == data["update_uuid"]).first()
    if prev_update:
        # From the CryoAdmin interface, if the same uuid occurs, its because everything is the same
        return

    new_update = UpdateQueue(
        timestamp=datetime.now(UTC),
        url=data["url"],
        uuid=data["update_uuid"],
        alerts=data["alerts"],
    )
    session.add(new_update)
    session.commit()


def delete_updates(session: Session, updates: list[UpdateQueue]) -> None:
    for update in updates:
        session.delete(update)
    session.commit()


def clear_old_updates(session: Session, expiry_time: float = 1800) -> None:
    """Clear old updates that have been locked after some expiry time.
    This is done to clean up old requests that failed for some reason after they were locked
    by a process, but before they were fully processed.

    Args:
        session (Session): Active database session
        expiry_time (float, optional): Expiry time of the locked updates in seconds. Defaults to 1800.
    """
    expiry_datetime = datetime.now(UTC) - timedelta(seconds=expiry_time)
    (
        session.query(UpdateQueue)
        .filter(UpdateQueue.locked)
        .filter(UpdateQueue.locked_timestamp < expiry_datetime)
        .delete()
    )
    session.commit()
