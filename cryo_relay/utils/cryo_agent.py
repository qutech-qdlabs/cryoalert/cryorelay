def get_alert_update_url(root_url: str) -> str:
    return f"{root_url}/api/v1/alerts/update_alert_definitions/"
