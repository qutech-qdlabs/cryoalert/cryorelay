from abc import ABC, abstractmethod

from cryo_relay.utils.base_process import BaseProcess


class AbstractFactory(ABC):
    @abstractmethod
    def create(self, idx: int, config: dict) -> BaseProcess: ...


class Warehouse:
    def __init__(self) -> None:
        self._process_list: list[BaseProcess] = []

    def add(self, process: BaseProcess) -> None:
        self._process_list.append(process)

    def start(self) -> None:
        for process in self._process_list:
            process.start()

    def stop(self) -> None:
        for process in self._process_list:
            process.stop()

    def join(self) -> None:
        for process in self._process_list:
            process.join()

        self._process_list = []


class Manager:
    def __init__(self, factory: AbstractFactory) -> None:
        self.warehouse = Warehouse()
        self.factory = factory

    def create_single_instance(self, idx: int, config: dict) -> None:
        process = self.factory.create(idx, config)
        self.warehouse.add(process)

    def create(self, config: dict) -> None:
        for idx in range(config.get("number_of_instances", 1)):
            self.create_single_instance(idx, config)

    def start(self) -> None:
        self.warehouse.start()

    def stop(self) -> None:
        self.warehouse.stop()

    def join(self) -> None:
        self.warehouse.join()
