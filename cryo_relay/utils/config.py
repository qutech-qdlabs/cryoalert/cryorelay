import os
from pathlib import Path
from typing import TypedDict

import toml


class _PollerConfig(TypedDict):
    poll_interval: int


class _AlertUpdaterConfig(TypedDict):
    number_of_instances: int
    poll_interval: int


class _AlertResponseUpdateConfig(TypedDict):
    number_of_instances: int
    poll_interval: int


class PollerConfig(TypedDict):
    log_level: str
    cryo_admin_url: str
    cryo_admin_api_key: str
    poll_interval: int


class AlertUpdaterConfig(TypedDict):
    log_level: str
    cryo_admin_url: str
    cryo_admin_api_key: str
    number_of_instances: int
    poll_interval: int


class AlertResponseUpdateConfig(TypedDict):
    log_level: str
    cryo_admin_url: str
    cryo_admin_api_key: str
    number_of_instances: int
    poll_interval: int


class Config(TypedDict):
    log_level: str
    cryo_admin_url: str
    cryo_admin_api_key: str
    poller: _PollerConfig
    alert_updater: _AlertUpdaterConfig
    alert_response_updater: _AlertResponseUpdateConfig


def get_poller_config(conf: Config) -> PollerConfig:
    return {
        "log_level": conf["log_level"],
        "cryo_admin_url": conf["cryo_admin_url"],
        "cryo_admin_api_key": conf["cryo_admin_api_key"],
        **conf["poller"],
    }


def get_alert_updater_config(conf: Config) -> AlertUpdaterConfig:
    return {
        "log_level": conf["log_level"],
        "cryo_admin_url": conf["cryo_admin_url"],
        "cryo_admin_api_key": conf["cryo_admin_api_key"],
        **conf["alert_updater"],
    }


def get_alert_response_updater_config(conf: Config) -> AlertResponseUpdateConfig:
    return {
        "log_level": conf["log_level"],
        "cryo_admin_url": conf["cryo_admin_url"],
        "cryo_admin_api_key": conf["cryo_admin_api_key"],
        **conf["alert_response_updater"],
    }


def read_config(config_path: str) -> Config:
    full_config_path = Path(os.getcwd()) / Path(config_path)
    return toml.load(full_config_path)
