import logging
from functools import wraps

LOGGER = logging.getLogger(__name__)


def db_lock_required(func):
    @wraps(func)
    def wrapper(self, *args, **kwargs):
        if hasattr(self, "db_lock"):
            LOGGER.debug(f"{self}: Acquiring Database Lock")
            self.db_lock.acquire()
            LOGGER.debug(f"{self}: Acquired Database Lock")

        res = func(self, *args, **kwargs)

        if hasattr(self, "db_lock"):
            LOGGER.debug(f"{self}: Releasing Database Lock")
            self.db_lock.release()
            LOGGER.debug(f"{self}: Released Database Lock")

        return res

    return wrapper
