from typing import NotRequired, TypedDict


class AlertRule(TypedDict):
    name: str
    unit: str
    default_enabled: bool
    bounds_upper_default_enabled: bool
    bounds_upper_default_value: float
    bounds_lower_default_enabled: bool
    bounds_lower_default_value: float


class AlertCollection(TypedDict):
    name: str
    rules: list[AlertRule]


class AlertDefinitionUpdate(TypedDict):
    url: str
    update_uuid: str
    alerts: AlertCollection


class AlertUpdateResponse(TypedDict):
    ok: bool
    error_message: NotRequired[str]
