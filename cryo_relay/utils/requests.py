import logging

import requests

LOGGER = logging.getLogger(__name__)


def request(
    session: requests.Session,
    method: str,
    url: str,
    json: dict | None = None,
    headers: dict | None = None,
    params: dict | None = None,
    timeout: float = 5.0,
) -> tuple[requests.Response | None, str]:

    try:
        resp = session.request(method, url=url, json=json, headers=headers, params=params, timeout=timeout)
    except requests.exceptions.Timeout:
        return None, f"The server at `{url}` could not be reached."
    except Exception as exc:
        return None, f"Unknown Error: {exc}"

    if not (200 <= resp.status_code <= 299):
        return None, f"A response with status_code={resp.status_code} was returned"

    return resp, ""
