from sqlalchemy import JSON, Boolean, Column, DateTime, Integer, Text
from sqlalchemy.orm import declarative_base

DEFAULT_DB_PATH = "sqlite:///cryo_relay.db"

# Define the base for sqlalchemy models
Base = declarative_base()


class LastPollAction(Base):
    __tablename__ = "last_poll_action"

    id = Column(Integer, primary_key=True)
    timestamp = Column(DateTime, nullable=False)

    def get_formatted_latest_poll_time(self):
        return self.timestamp.strftime("%Y-%m-%dT%H:%M:%S")


class UpdateQueue(Base):
    __tablename__ = "update_queue"

    id = Column(Integer, primary_key=True)
    timestamp = Column(DateTime, nullable=False)

    url = Column(Text, nullable=False)
    uuid = Column(Text, nullable=False)
    alerts = Column(JSON)

    locked = Column(Boolean, default=False)
    locked_timestamp = Column(DateTime, nullable=True)


class UpdateResponseQueue(Base):
    __tablename__ = "update_response_queue"

    id = Column(Integer, primary_key=True)
    timestamp = Column(DateTime, nullable=False)

    uuid = Column(Text, nullable=False)
    success = Column(Boolean)
    error = Column(Text)

    locked = Column(Boolean, default=False)
    locked_timestamp = Column(DateTime, nullable=True)
