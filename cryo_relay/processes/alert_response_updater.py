import logging
import multiprocessing as mp
import time
from datetime import UTC, datetime
from multiprocessing.synchronize import Lock

import requests
from sqlalchemy import create_engine
from sqlalchemy.orm import Session

from cryo_relay.logic.update_response_queue import (
    delete_response_updates,
    get_latest_update_response,
    unlock_response_updates,
)
from cryo_relay.models import DEFAULT_DB_PATH, UpdateResponseQueue
from cryo_relay.utils.base_process import BaseProcess
from cryo_relay.utils.decorators import db_lock_required
from cryo_relay.utils.logging import init_process_logging
from cryo_relay.utils.requests import request

LOGGER = logging.getLogger(__name__)


class AlertResponseUpdaterProcess(BaseProcess):
    """
    A multiprocessing process for updating CryoAdmin of the response of CryoAgents when trying to update
    the alert definitions
    """

    def __init__(self, idx: int, config: dict, log_queue: mp.Queue, db_lock: Lock):
        super().__init__()

        # Add properties
        self.config = config
        self.log_queue = log_queue
        self.idx = idx
        self.db_lock = db_lock

        self.cryo_admin_url = self.config.get("cryo_admin_url")
        self.cryo_admin_api_key = self.config.get("cryo_admin_api_key")
        self.log_level = self.config.get("log_level", "INFO")
        self.poll_interval = self.config.get("poll_interval", 10)

        self.request_session = requests.Session()

    def init(self) -> None:
        pass

    def run(self) -> None:
        """
        Entrypoint of the AlertResponseUpdater
        """
        # Init logging again in new process
        init_process_logging(log_queue=self.log_queue, log_level=self.log_level)

        # Do any further initializion if needed
        self.init()

        # collect
        while not self.is_stopped:
            try:
                LOGGER.info(f"Starting AlertResponseUpdater{self.idx}")
                self._collect()
            except KeyboardInterrupt:
                pass
            except Exception as exc:
                LOGGER.error(exc, exc_info=True)
            finally:
                LOGGER.info(f"Stopped AlertResponseUpdater{self.idx}")
                try:
                    if not self.is_stopped:
                        time.sleep(5)
                except Exception:
                    pass

    def _collect(self):
        with Session(create_engine(DEFAULT_DB_PATH)) as session:
            last_scan_cycle_time = datetime(2000, 1, 1).astimezone(UTC)
            while not self.is_stopped:
                if (datetime.now(UTC) - last_scan_cycle_time).total_seconds() > self.poll_interval:
                    LOGGER.debug(f"Time to collect after {(datetime.now(UTC) - last_scan_cycle_time).total_seconds()}")
                    self.collect(session)
                    LOGGER.debug(f"Finished collecting!")
                    last_scan_cycle_time = datetime.now(UTC)
                else:
                    time.sleep(0.1)  # short sleep to not use all CPU

    def collect(self, session: Session) -> None:
        # Get latest ResponseUpdates from the Queue
        response_updates = self._get_latest_response_updates(session)

        # Inform CryoAdmin of the responses
        success = self._update_cryo_admin(response_updates)
        if success:
            # Delete the processed ResponseUpdates
            self._delete_processed_response_updates(session, response_updates)
        else:
            # Unlock the response updates in the database to try again later
            self._unlock_unprocessed_response_updates(session, response_updates)

        # Quickly run a scan for any response updates that are locked longer than some time.

    @db_lock_required
    def _get_latest_response_updates(self, session: Session) -> list[UpdateResponseQueue]:
        # Fetch a couple of updates and lock those
        # TODO fetch more than one, can be set from config
        LOGGER.debug(f"{self}: Fetching latest unlocked response_updates")
        response_updates = get_latest_update_response(session, limit=10, lock=True)
        LOGGER.debug(f"{self}: Fetched {len(response_updates)} response_updates")
        return response_updates

    def _update_cryo_admin(self, response_updates: list[UpdateResponseQueue]) -> bool:
        _, error_message = request(
            session=self.request_session,
            method="POST",
            url=self.cryo_admin_url,
            headers={"Authorization": f"Api-Key {self.cryo_admin_api_key}"},
            json=[
                {
                    "uuid": response_update.uuid,
                    "success": response_update.success,
                    "error": response_update.error,
                }
                for response_update in response_updates
            ],
        )

        if error_message:
            LOGGER.error(error_message)
            return False

        return True

    @db_lock_required
    def _delete_processed_response_updates(self, session: Session, response_updates: list[UpdateResponseQueue]) -> None:
        # Delete the processed response_updates
        LOGGER.debug(f"{self}: Deleting processed response_updates")
        delete_response_updates(session, response_updates)
        LOGGER.debug(f"{self}: Deleted processed response_updates")

    @db_lock_required
    def _unlock_unprocessed_response_updates(
        self, session: Session, response_updates: list[UpdateResponseQueue]
    ) -> None:
        # Delete the processed response_updates
        LOGGER.debug(f"{self}: Unlocking unprocessed response_updates")
        unlock_response_updates(session, response_updates)
        LOGGER.debug(f"{self}: Unlocked unprocessed response_updates")

    def __str__(self) -> str:
        return f"<{self.__class__.__name__}(idx={self.idx})>"

    __repr__ = __str__
