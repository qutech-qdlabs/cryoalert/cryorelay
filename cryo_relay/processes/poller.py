import logging
import multiprocessing as mp
import time
from datetime import UTC, datetime

import requests
from sqlalchemy import create_engine
from sqlalchemy.orm import Session

from cryo_relay.logic.last_poll_action import (
    get_latest_poll_action,
    update_latest_poll_action,
)
from cryo_relay.logic.update_queue import add_latest_update
from cryo_relay.models import DEFAULT_DB_PATH
from cryo_relay.utils.base_process import BaseProcess
from cryo_relay.utils.logging import init_process_logging
from cryo_relay.utils.requests import request
from cryo_relay.utils.types import AlertDefinitionUpdate

LOGGER = logging.getLogger(__name__)


class PollerProcess(BaseProcess):
    """
    A MultiProcess Process for polling a CryoAdmin instance for alert definition updates for the fridges
    configured to be connected to the relay this CryoRelay instance is running on
    """

    def __init__(self, config: dict, log_queue: mp.Queue):
        super().__init__()

        # Add properties
        self.config = config
        self.log_queue = log_queue

        self.cryo_admin_url = self.config.get("cryo_admin_url")
        self.cryo_admin_api_key = self.config.get("cryo_admin_api_key")
        self.poll_interval = self.config.get("poll_interval", 60)
        self.log_level = self.config.get("log_level", "INFO")

        self.request_session = requests.Session()

    def init(self) -> None:
        pass

    def run(self) -> None:
        """
        Entrypoint of process to run the webserver
        """
        # Init logging again in new process
        init_process_logging(log_queue=self.log_queue, log_level=self.log_level)

        # Do any further initializion if needed
        self.init()

        # collect
        while not self.is_stopped:
            try:
                LOGGER.info(f"Starting Poller")
                self._collect()
            except KeyboardInterrupt:
                pass
            except Exception as exc:
                LOGGER.error(exc, exc_info=True)
            finally:
                LOGGER.info("Stopped Poller")
                try:
                    if not self.is_stopped:
                        time.sleep(5)
                except Exception:
                    pass

    def _collect(self):
        with Session(create_engine(DEFAULT_DB_PATH)) as session:
            last_scan_cycle_time = datetime(2000, 1, 1).astimezone(UTC)
            while not self.is_stopped:
                if (datetime.now(UTC) - last_scan_cycle_time).total_seconds() > self.poll_interval:
                    LOGGER.debug(f"Time to collect after {(datetime.now(UTC) - last_scan_cycle_time).total_seconds()}")
                    self.collect(session)
                    LOGGER.debug(f"Finished collecting!")
                    last_scan_cycle_time = datetime.now(UTC)
                else:
                    time.sleep(0.1)  # short sleep to not use all CPU

    def collect(self, session) -> None:
        # Get the last_poll_time, if exists.
        latest_poll_action = get_latest_poll_action(session)
        if latest_poll_action:
            last_poll_time = latest_poll_action.get_formatted_latest_poll_time()
        else:
            last_poll_time = datetime(2000, 1, 1).astimezone(UTC).strftime("%Y-%m-%dT%H:%M:%S")
        LOGGER.debug(f"Last Poll time was at: {last_poll_time}")

        # Note the time before the polling, to have some overlap for next poll time
        current_poll_time = datetime.now(UTC)

        # Get any updated alert definitions for the fridge assigned to the relay this CryoRelay instance is
        # running on
        resp, err_msg = request(
            session=self.request_session,
            method="GET",
            url=self.cryo_admin_url,
            headers={"Authorization": f"Api-Key {self.cryo_admin_api_key}"},
            params={"last_poll_time": last_poll_time},
        )
        if err_msg:
            return

        # Extract data from the response, and write it into to UpdateQueue
        data: list[AlertDefinitionUpdate] = resp.json()
        LOGGER.debug(f"Retrieved {data=}")
        for update in data:
            add_latest_update(session, update)

        # Update the "LastPollAction" row
        update_latest_poll_action(session, new_poll_time=current_poll_time)
