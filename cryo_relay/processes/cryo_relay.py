import logging
import multiprocessing as mp
import threading
from datetime import UTC, datetime
from time import sleep

from cryo_relay.factories.alert_response_updater import AlertResponseUpdaterFactory
from cryo_relay.factories.alert_updater import AlertUpdaterFactory
from cryo_relay.processes.poller import PollerProcess
from cryo_relay.utils.config import (
    Config,
    get_alert_response_updater_config,
    get_alert_updater_config,
    get_poller_config,
)
from cryo_relay.utils.factory_management import Manager
from cryo_relay.utils.logging import logger_thread

LOGGER = logging.getLogger(__name__)


class CryoRelay:
    """Class that controls the starting of the processes used by the CryoRelay"""

    def __init__(
        self,
        config: Config,
        active_poller: bool,
        active_updater: bool,
        active_response_updater: bool,
    ) -> None:
        self._process_health_interval = 60
        self.active_poller = active_poller
        self.active_updater = active_updater
        self.active_response_updater = active_response_updater

        if not self.active_poller:
            LOGGER.info("Poller Process has been disabled")

        if not self.active_updater:
            LOGGER.info("Updater Processes have been disabled")

        if not self.active_response_updater:
            LOGGER.info("Response Updater Processes have been disabled")

        self.log_queue = mp.Queue()
        self._log_thread = threading.Thread(target=logger_thread, args=(self.log_queue,))

        self._poller = PollerProcess(config=get_poller_config(config), log_queue=self.log_queue)
        self._alert_updater_manager = Manager(factory=AlertUpdaterFactory(log_queue=self.log_queue, db_lock=mp.Lock()))
        self._alert_response_updater_manager = Manager(
            factory=AlertResponseUpdaterFactory(log_queue=self.log_queue, db_lock=mp.Lock())
        )

        # Creation of subprocesses
        if self.active_updater:
            self._alert_updater_manager.create(config=get_alert_updater_config(config))

        if self.active_response_updater:
            self._alert_response_updater_manager.create(config=(get_alert_response_updater_config(config)))

    def setup(self) -> None:
        """Method that runs only once, before the first run of the loop"""

        # Start logging thread
        self._log_thread.start()

        # Start AlertUpdaterProcesses
        if self.active_updater:
            LOGGER.debug("Starting the Updater processes")
            self._alert_updater_manager.start()

        if self.active_response_updater:
            LOGGER.debug("Starting the ResponseUpdater processes")
            self._alert_response_updater_manager.start()

        # Start Poller Process
        if self.active_poller:
            LOGGER.debug("Starting the Poller process")
            self._poller.start()

    def loop(self) -> None:
        """Method that run forever, unless aborted."""
        # Get start time of this scan cycle
        LOGGER.debug("Start new scan cycle")
        dt_start = datetime.now(UTC)

        # SLeep to not over saturate this process if queue was empty
        sleep_time = max([0, self._process_health_interval - (datetime.now(UTC) - dt_start).total_seconds()])
        LOGGER.debug(f"sleeping for {sleep_time} seconds")
        sleep(sleep_time)
        LOGGER.debug(f"slept for {sleep_time} seconds")

    def run(self) -> None:
        """Method for starting the processes and loop forever ensuring
        processes are alive."""

        # First, run the setup once
        self.setup()

        # run the loop forever
        LOGGER.info(f"Started {self.__class__.__name__}")
        try:
            while True:
                self.loop()
        except KeyboardInterrupt:
            # Stop the processes
            if self.active_updater:
                self._alert_updater_manager.stop()

            if self.active_response_updater:
                self._alert_response_updater_manager.stop()

            if self.active_poller:
                self._poller.stop()

            # Wait untill both processes have stopped
            if self.active_updater:
                self._alert_updater_manager.join()

            if self.active_response_updater:
                self._alert_response_updater_manager.join()

            if self.active_poller:
                self._poller.join()

            # Stop the logging thread to finish
            self.log_queue.put(None)
            self._log_thread.join()

            # Print something on the terminal
            LOGGER.info(f"Aborting {self.__class__.__name__}!")
