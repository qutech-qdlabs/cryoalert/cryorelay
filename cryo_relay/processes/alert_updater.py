import logging
import multiprocessing as mp
import time
from datetime import UTC, datetime
from multiprocessing.synchronize import Lock

import requests
from sqlalchemy import create_engine
from sqlalchemy.orm import Session

from cryo_relay.logic.update_queue import (
    clear_old_updates,
    delete_updates,
    get_latest_updates,
)
from cryo_relay.logic.update_response_queue import add_latest_update_response
from cryo_relay.models import DEFAULT_DB_PATH, UpdateQueue
from cryo_relay.utils.base_process import BaseProcess
from cryo_relay.utils.cryo_agent import get_alert_update_url
from cryo_relay.utils.decorators import db_lock_required
from cryo_relay.utils.logging import init_process_logging
from cryo_relay.utils.requests import request
from cryo_relay.utils.types import AlertUpdateResponse

LOGGER = logging.getLogger(__name__)


class AlertUpdaterProcess(BaseProcess):
    """
    A multiprocessing process for updating CryoAgents with new alert definitions
    """

    def __init__(self, idx: int, config: dict, log_queue: mp.Queue, db_lock: Lock):
        super().__init__()

        # Add properties
        self.config = config
        self.log_queue = log_queue
        self.idx = idx
        self.db_lock = db_lock

        self.request_session = requests.Session()

        self.log_level = self.config.get("log_level", "INFO")
        self.poll_interval = self.config.get("poll_interval", 10)

    def init(self) -> None:
        pass

    def run(self) -> None:
        """
        Entrypoint of the AlertUpdater
        """
        # Init logging again in new process
        init_process_logging(log_queue=self.log_queue, log_level=self.log_level)

        # Do any further initializion if needed
        self.init()

        # collect
        while not self.is_stopped:
            try:
                LOGGER.info(f"Starting AlertUpdater{self.idx}")
                self._collect()
            except KeyboardInterrupt:
                pass
            except Exception as exc:
                LOGGER.error(exc, exc_info=True)
            finally:
                LOGGER.info(f"Stopped AlertUpdater{self.idx}")
                try:
                    if not self.is_stopped:
                        time.sleep(5)
                except Exception:
                    pass

    def _collect(self):
        with Session(create_engine(DEFAULT_DB_PATH)) as session:
            last_scan_cycle_time = datetime(2000, 1, 1).astimezone(UTC)
            while not self.is_stopped:
                if (datetime.now(UTC) - last_scan_cycle_time).total_seconds() > self.poll_interval:
                    LOGGER.debug(f"Time to collect after {(datetime.now(UTC) - last_scan_cycle_time).total_seconds()}")
                    self.collect(session)
                    LOGGER.debug(f"Finished collecting!")
                    last_scan_cycle_time = datetime.now(UTC)
                else:
                    time.sleep(0.1)  # short sleep to not use all CPU

    def collect(self, session: Session) -> None:
        updates = self._get_latest_updates(session)

        # For each update
        for update in updates:
            self._update_cryo_agent(session, update)

        # Delete update from UpdateQueue Table
        self._delete_processed_updates(session, updates)

        # Quickly run a scan for any updates that are locked longer than some time
        self._clear_old_updates(session)

    @db_lock_required
    def _get_latest_updates(self, session: Session) -> list[UpdateQueue]:
        # Fetch a couple of updates and lock those
        # TODO fetch more than one, can be set from config
        LOGGER.debug(f"{self}: Fetching latest unlocked updates")
        updates = get_latest_updates(session, limit=1, lock=True)
        LOGGER.debug(f"{self}: Fetched {len(updates)} updates")

        return updates

    def _update_cryo_agent(self, session: Session, update: UpdateQueue) -> None:
        # send updates to the CryoAgents
        resp, error_message = request(
            session=self.request_session,
            method="POST",
            url=get_alert_update_url(update.url),
            json=update.alerts.get("rules", []),
        )
        if error_message:
            data = {"ok": False, "error_message": error_message}
        else:
            data: AlertUpdateResponse = resp.json()

        # Store the results of the updates(success/failure)
        add_latest_update_response(session, data, update.uuid)

    @db_lock_required
    def _delete_processed_updates(self, session: Session, updates: list[UpdateQueue]) -> None:
        # Delete the processed updates
        LOGGER.debug(f"{self}: Deleting processed updates")
        delete_updates(session, updates)
        LOGGER.debug(f"{self}: Deleted processed updates")

    @db_lock_required
    def _clear_old_updates(self, session: Session):
        # Clear the old updates
        LOGGER.debug(f"{self}: Deleting old updates")
        clear_old_updates(session)
        LOGGER.debug(f"{self}: Deleted old updates")

    def __str__(self) -> str:
        return f"<{self.__class__.__name__}(idx={self.idx})>"

    __repr__ = __str__
