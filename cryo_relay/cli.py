import os
import shutil
from datetime import datetime
from importlib import resources
from pathlib import Path

import click
from jinja2 import Environment, PackageLoader
from pyfiglet import Figlet
from sqlalchemy import create_engine

from cryo_relay.models import DEFAULT_DB_PATH, Base
from cryo_relay.processes.cryo_relay import CryoRelay
from cryo_relay.utils.config import read_config
from cryo_relay.utils.logging import init_logging


def _print_logo():
    figure = Figlet(font="slant")
    print("=" * 30)
    print(figure.renderText("CryoRelay"))
    print("=" * 30)


@click.group()
def cli():
    """
    CLI commands to run the CryoRelay
    """


@cli.command()
@click.option(
    "--log_level",
    default="INFO",
    type=click.Choice(["DEBUG", "INFO", "WARNING", "ERROR"]),
    prompt="What loglevel do you want to set?",
)
@click.option(
    "--cryo_admin_url",
    default="https://cryoadmin.tnw.tudelft.nl/api/v1/alerts/",
    prompt="What URL should the CryoRelay use for polling new alert definitions?",
)
@click.option("--cryo_admin_api_key", prompt="Please provide the API-key of this relay for CryoAdmin access.")
def init(log_level: str, cryo_admin_url: str, cryo_admin_api_key: str):
    """
    CLI command to init a folder with the correct structure to deploy a CryoRelay
    """

    # Get current working directory
    cwd = Path(os.getcwd())

    # Get template environment
    env = Environment(loader=PackageLoader("cryo_relay", package_path="config"))

    # Set default template_kwargs
    filename = "config.toml"
    template_kwargs = {
        "filename": filename,
        "date": datetime.now().strftime("%d-%m-%Y"),
        "log_level": log_level,
        "cryo_admin_url": cryo_admin_url,
        "cryo_admin_api_key": cryo_admin_api_key,
    }

    # Render and write config
    rendered_template = env.get_template("config.tpl-toml").render(**template_kwargs)
    with open(cwd / Path(filename), "w", encoding="utf-8") as template_file:
        template_file.write(rendered_template)

    # Setup log folder if not exists
    logs_path = Path.cwd() / "logs"
    if not logs_path.exists():
        logs_path.mkdir()

    # Setup run script if not exists
    # Copy the start-up script(s)
    for file in resources.files("cryo_relay.run").iterdir():
        if file.is_file() and file.name.endswith(".bat") and not (Path.cwd() / file.name).exists():
            shutil.copyfile(file, Path.cwd() / file.name)


@cli.command()
@click.option(
    "--config_path",
    default="config.toml",
    type=click.Path(exists=True),
    help="Path of custom configuration toml file",
)
@click.option("--no-poller", "no_poller", flag_value=True, default=False)
@click.option("--no-updater", "no_updater", flag_value=True, default=False)
@click.option("--no-response-updater", "no_response_updater", flag_value=True, default=False)
def start(config_path: str, no_poller: bool, no_updater: bool, no_response_updater: bool):
    """
    CLI command to start the CryoRelay
    """

    # read config
    conf = read_config(config_path=config_path)

    # Init logging
    init_logging(log_level=conf["log_level"])

    # print logo
    _print_logo()

    # Initialize database if needed
    Base.metadata.create_all(create_engine(DEFAULT_DB_PATH))

    # Create the CryoRelay main Process
    cryo_relay = CryoRelay(
        conf,
        active_poller=not no_poller,
        active_updater=not no_updater,
        active_response_updater=not no_response_updater,
    )

    # Run the CryoRelay
    cryo_relay.run()
