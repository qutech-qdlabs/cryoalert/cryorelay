# Installation

## Setup and Install

First, create a folder in which the database and log files can live. Then, go into this folder in the terminal.

```cmd
mkdir example
cd example
```

It is prefered to run the cryo-relay in a virtual environment, as not to interfer with any other python installations on the machine.
This can be done using the following command (assuming python has been installed on the machine):

```cmd
python -m venv .venv
```

To work in the virtual environment, it needs to be activated.
This can be done using:

```cmd
.\.venv\Scripts\activate.bat 
```

It is then possible to install the cryo_relay in the virtual environment:

```cmd
pip install cryo-relay --extra-index-url https://gitlab.tudelft.nl/api/v4/projects/7052/packages/pypi/simple
```

## Configuration

Before running CryoRelay, it needs to be configured first. See the [configuration](config.md) documentation on how to do this.

## Run in Terminal

The CryoRelay can be run using the terminal using:

```cmd
cryo_relay start 
```

## Run via Script and Automatic start

After the first start, a (windows) batch script will be copied to the root folder where CryoRelay was installed (the folder containing the `.venv` folder). This script can be used to start CryoRelay. Furthermore, a shortcut to this script can be made and copied into the startup folder of the PC (found at `C:\ProgramData\Microsoft\Windows\Start Menu\Programs\StartUp`). This ensures CryoRelay starts automatically when the computer has restarted (after login, if that is needed).
