# Configuration

CryoRelay can be configured using the `cryo_relay init` CLI command. It will prompt the user for the configuration information needed to succesfully deploy CryoRelay. The options are shown below. Note that the user will be prompted for these (except the `--help`) and do not need to be provided when initially running the command. Note that the `cryo_admin_api_key` parameter can be obtained from CryoAdmin.

```cmd
> cryo_relay init --help
Usage: cryo_relay init [OPTIONS]

  CLI command to init a folder with the correct structure to deploy a CryoRelay

Options:
  --log_level [DEBUG|INFO|WARNING|ERROR]
  --cryo_admin_url TEXT
  --cryo_admin_api_key TEXT
  --help                          Show this message and exit.   
```
