# Overview

The CryoRelay application is a relatively simple application that collectors and forwards alert-collections to the various fridges connected to the relay on which CryoRelay is installed, when such an alert-collection is updated by a user within CryoAdmin.

## Sequence

The sequence diagram shown below captures the sequence of steps where the CryoRelay application is involved. It polls for Alert Collection updates from CryoAdmin. If atleast one fridge assigned to the current relay has an alert update, the complete list of alert rules for that fridge will be send as a response. CryoRelay will then loop over the updates for all fridges in the response and inform CryoAdmin about the success/failure of the update to that fridge.

![Sequence](./img/CryoRelaySequence.png)

The data returned by CryoAdmin should have the following structure:

```json
[
    {
        "url": <some_address>,
        "update_uuid": <some_uuid>,
        "alerts": [
            {
                "alert_name": ...,
                "unit": ...,
                "default_enabled": ...,
                "bounds": None
            },
            {
                "alert_name": ...,
                "unit": ...,
                "default_enabled": ...,
                "bounds": {
                    "upper": {
                        "default_enabled": ...,
                        "default_value":...,
                    },
                    "lower": {
                        "default_enabled": ...,
                        "default_value": ...,
                    }
                }
            },
            {
                ...
            }
        ]
    },
    {
        ...
    }
]
```

## Design

To ensure the updates happen in a robust manner, multiple processes and an internal SQLite queue are used. This design will ensure that each alert update happens at least once. Because the alert updates are idempotent within CryoAgent, there are no consequences if an alert update happens twice. The same applies to the "AlertUpdateReponses" within CryoAdmin.

![Design](./img/CryoRelayDesign.png)
